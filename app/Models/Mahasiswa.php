<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mahasiswa extends Model
{
    //
    protected $table = 'Mahasiswa';

    protected $fillable = ['nama', 'nim', 'fakultas', 'jurusan', 'no_hp', 'no_wa', 'user_id'];

    protected $guarded = [];
}
