<?php

namespace App\Http\Controllers\Auth;

//use Illuminate\Http\Request;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Mahasiswa;
use App\Models\Role;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(RegisterRequest $request)
    {
        //

        //dd($request);
        // request()->validate(
        //     [
        //         'nama' => ['required', 'string', 'max:255'],
        //         'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        //         'password' => ['required', 'string', 'min:8'],
        //         'nim' => ['required', 'string', 'max:20'],
        //         'fakultas' => ['required', 'string', 'max:255'],
        //         'jurusan' => ['required', 'string', 'max:255'],
        //         'no_hp' => ['required', 'string', 'max:20'],
        //         'no_wa' => ['required', 'string', 'max:20'],
        //     ]
        // );

        $user = User::create([
                    'name' => request('nama'),
                    'email' => request('email'),
                    'password' => bcrypt(request('password'))
                ]);
        //$user = App\User::find(1);

        $user->roles()->attach('2');

        $mahasiswa = Mahasiswa::create([
                'nama' => request('nama'),
                'nim' => request('nim'),
                'fakultas' => request('fakultas'),
                'jurusan' => request('jurusan'),
                'no_hp' => request('no_hp'),
                'no_wa' => request('no_wa'),
                'user_id' => $user->id
        ]);

        return response('Thanks for Your Register As Mahasiswa');
    }
}
