<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                    'nama' => ['required', 'string', 'max:255'],
                    'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                    'password' => ['required', 'string', 'min:8'],
                    'nim' => ['required', 'string', 'max:20'],
                    'fakultas' => ['required', 'string', 'max:255'],
                    'jurusan' => ['required', 'string', 'max:255'],
                    'no_hp' => ['required', 'string', 'max:20'],
                    'no_wa' => ['required', 'string', 'max:20'],
                ];
    }
}
