<?php

use Illuminate\Database\Seeder;
use App\Models\Mahasiswa;

class MahasiswaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $mahasiswa = Mahasiswa::create([
            'nama' => 'Mahasiswa Abadi',
            'nim'  => '0812001',
            'fakultas' => 'Teknologi Informasi',
            'jurusan' => 'Teknik Informatika',
            'no_hp' => '0812',
            'no_wa' => '0812',
            'user_id' => 2
        ]);
    }
}
