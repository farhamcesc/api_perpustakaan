<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $user_admin = User::create([
                        'name' => 'Admin Perpustakaan',
                        'email' => 'aadmin@perpus.com',
                        'password' => bcrypt('123456')
                    ]);
        $user_mahasiswa = User::create([
                        'name' => 'Mahasiswa Abadi',
                        'email' => 'maha1@perpus.com',
                        'password' => bcrypt('123456')
                    ]);
    }
}
