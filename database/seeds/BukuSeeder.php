<?php

use Illuminate\Database\Seeder;
use App\Models\Buku;

class BukuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $buku1 = Buku::create([
            'kode_buku' => '001',
            'judul' => 'Pemrogramman Laravel',
            'pengarang' => 'Lary Otwel',
            'tahun_terbit' => 2016
        ]);

        $buku2 = Buku::create([
            'kode_buku' => '001',
            'judul' => 'Pemrogramman Java',
            'pengarang' => 'Darwin',
            'tahun_terbit' => 2005
        ]);

        $buku3 = Buku::create([
            'kode_buku' => '001',
            'judul' => 'Pemrogramman PHP',
            'pengarang' => 'Elon Musk',
            'tahun_terbit' => 2013
        ]);

        $buku3 = Buku::create([
            'kode_buku' => '001',
            'judul' => 'Pemrogramman Javascript',
            'pengarang' => 'Shandika Galih',
            'tahun_terbit' => 2010
        ]);
    }
}
