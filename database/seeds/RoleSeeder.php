<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $role_admin = Role::create([
            'name' => 'admin'
        ]);

        $role_mahasiswa = Role::create([
            'name' => 'mahasiswa'
        ]);
    }
    
}
