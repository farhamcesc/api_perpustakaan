<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>


## Tugas Membuat API Perpustakaan

Tugas untuk hari ke-3 dan hari ke-4 adalah membuat API menggunakan Laravel dengan tema Perpustakaan.

1. Tidak perlu membuat tampilan UI
2. Otentikasi menggunakan JWT
3. Terdapat 2 roles, yaitu admin dan mahasiswa
4. Data Mahasiswa yang harus ada adalah : nama, nim , fakultas, jurusan, no hp , no WA
5. Data Buku yang harus ada adalah : kode buku (unique), judul, pengarang, tahun terbit
6. Data Pinjaman yang harus ada adalah : tanggal peminjaman, tanggal batas akhir peminjaman, tanggal pengembalian, mahasiswa yang meminjam, status ontime (tipe data boolean)
7. Semua route bisa diakses oleh Admin.
8. Route untuk mengupdate tanggal pengembalian dan mengubah status ontime hanya bisa diakses oleh Admin saja.
9. Buatkan juga ERD nya

## Rancangan ERD

<p align="center"><a href="https://viewer.diagrams.net/?title=ERD%20API%20Perpustakaan#R7Z1bc5s4FMc%2FTWZ2H7rDxfjyGDtuu9Ok7aTdze5TRjayTYORB%2BTY2U%2B%2Fwgh8kWRjYwkRmEk7IEAG%2FY9%2BiKPD4cYezNefQrCYPSAX%2BjeW4a5v7LsbyzJblnUT%2FxnuW1LS6dCCaei5dKdtwQ%2FvP0gLDVq69FwY7e2IEfKxt9gvHKMggGO8VwbCEK32d5sgf%2F9XF2AKmYIfY%2BCzpU%2Bei2dJadcxtuWfoTedpb9sGnTLHKQ704JoBly02imyhzf2IEQIJ0vz9QD6ceOl7ZIc91GwNTuxEAY4zwFP97d3w8nD0w%2FL%2Bmo9PRkrb%2Fjhg2naST2vwF%2FSS6ani9%2FSNoAuaRK6Sn7Mw2%2BP0AfYQ8Fwu6U%2FQQGm%2Bpnk4vozPPfjZbIIA%2Fc21oKsDh9RAH%2BiBxAQg%2BgnPxXXL7woWhShZTiGR66k41DrAOEU4iM79lpZ4xOrhWgOcfhGDlxt5c1EnO1KmxaGm4t%2F3T9lQO1smlWY%2FcZ35JGLsQzaJ%2BzUtGmPaNnGfhXJpdKjdgU9qKh1UJF9WFHSFExFZGHnwrdFG3s5y3Za78R27Lym0ynXdNrWgeKtC02nc1CRdViRdNNpqzadOQjcb0F8REQuDvM2XNGizF5Ok7Ibi7qORdlmmTCCaw%2F%2Fk%2B5Elv8ly8YfDl27ixvJSFfe6Mo1zc3s5rQ309HL4JxrGVxLscF12Jtff%2FmyZGyODPsW8SIGIz%2BjDzUvOzYDMnLFwAtgSM1njHwfLCJvs3tSMvN89x68oSVOK0rX%2BhNvDd3HZJgb70uM855UFlEb29gyPZl4M%2FC9aUCWx8TE4l%2FshzAi53IPIkz3mHi%2BP0A%2BCjenb7sAdifjzXmH6AXubGmPu3A0yez4FYYYro9bMmt29IDevpZdurpjlJbNMcpsbM2zvz3Bz1bXOU2TVFlymdgD%2FiN5%2FADBdCPyvoaxEG6IFj%2FTzhkXLGLDhOHwlTRRptZO2wdoc0%2FCaEE3%2BnCSHjtCGKN5qjhtj6zSTVs4ffJHWmcQU8ghpzsg6%2BZ2nfzFu4d4gAIiLfA2QkFiBysY20I%2BVY%2F0DFbr9BaTU9zD0ez1tGXHHt%2B%2FnKfu5lkTbNU9WzjjQLjDfopIw0%2F8zY1m5rkuDIopchr6ahXoMAr8eVeyAikZk3370QKMvWB6nxzZPpDIUSbR%2BkbYaXi3aXmSdSsBxENVqwDEdulA7BXU9uqdEbpeWp8EGnb1omE62tlp%2FhfkwufRZjhpfCD%2F%2Fr59HHy%2BfSRLv5F%2FpvG77rBUo5gucOzmePZs4HgRHHtlw7Fr1QqOXcGDWmlwZOcnfi3dpS8AI%2FmvZmwUCaYNG3NMEjRsvIiNplU6HIu6SSoGx9NTh2qbn%2FVkLGAwBSFpYC4gLcepISEFqmlDSNYb0hDySoR0SidkUb9JxQh5eoZcbfOzrg0MZsvgmXSHkYe5kGzVEJEC2XRBZI91kTSIvBIi8062yRO3qPekWojsGXw9ykJkTzsHh2L4iQTRBX6sPn9FMIyEIjWRBkfUzOJY0tnoDitvhsS9AJieLH1zROBqcG%2FTMtTAOnpr4%2FRcrrTSei7r%2BnrngQanoxmV3tpY71TdwwwEAolvddkTtBLBckQFa4BCLYf5x1HIiTFQi8KiPq5qjfHbenGQ9UEFYA51J6ESSXQhX9EQnIZ8%2BQMI1JIvfUurJujr6YU%2Bk3UuwTnw%2FHrBTyCKLvAzi7qgGvqdESKgGH9F3RvVwp8p0KM0%2FrE%2BiAWIohUK3XohUCSMNgysxhtHVWQgJwhAMQNZv8bHsh2BOSi47yu8vOdp9vaRyfoiQuTDZ6%2BSRJQskjZ4ZH0YD2AGIi9aAaFszUzYEUmddOYrnS3hxUpZHI0taTNhZjW8IFpOhZmCd2SOvHXLE1feLDbrBnnnk2GmZq4Qi3WF1H06TCTRkbteT6lkjWNEFhB5b92qBWK9HCOWZo4Ri3WMBGBOxpLNaxPHBdOGjY3DRBYbeS%2FdqmVj0UCQirFRMw%2BJxXpIAm%2FORaPp1A2L5ztK1GKxSdQiC4vc923VcrFemVoszTK12KwPYwJelj4mv9SMG0%2BIpgsg8yQKbQB5GSDzupDliVvUYVItQNqaZWuxedlawmUEAhEfawZHkWDawLHJ1iINjrwXbdXCsV7ZWmzNsrXYrFMjQM%2BzRfNcfUwtbcjYZGmRRcbsYz%2FlkbFeWVpszbK02Kxbg5BxxZ%2BMqR8Zz07OopaMLdYr0pDxSmRslU3GFustqVHoatL8%2BoCyxbo3lhEM6x26KhJJGz5WI8FHJfnIyd7Cj1mVJm5Rh0m1Ro6tyzJ8yGt%2B7VwaigeHIkHOhJ80gXos%2FOLPZL3b2P3JZGKNubH7bnvUdtp5xT7uJ84%2Bdib%2BYJbNy1EmUefGcXxxH%2B4J%2BrA4dJ8rrrzcFCxk49D9wccv5nkaVyyAP8eXgttKdWAdyPE3Yiow%2BpcbxS%2FSab2v0e4YsaVUt2q4jrV8rekEGznaKmYj6zmmbCx7ICqZjaddyGrZyLqQK%2BIZkczG487j0tmY5WPVHI5VHDhywvjVwtE06pXamxqzPlQ0DdZhjKf%2B8wLOveAXmGdxWXe3P4e6g1KRVvqQsfEbyyIjL5JfNRrr5Tim1qwTGlmvRozGEcAgegYvMy9sMHlEN30wWY2n60pikhPPrxqT9YrMotasEybZB%2BtkBBlM4XxEaFRrOOr%2BdF04%2BXADRyEc8066SVS3Zo%2FXpm6P15zExREGeBk9owB7cYr9DRn7377dD2%2B%2F1gyOIrX0gSM7cnxEvvi7CBUNSVCTTtBObzUUj07uD2s58gSuRi4ELSfeaO84JypBcVJdTrbI955RUBQOWVoqXU5Wx9rnFBSJpE0qXU4iSB2xqOVDwSkslv6hLTMVsy4PBaJxSXlMZOdtksyCelNRkSz6ULCJWJVGwfI%2FulU4O2TVKKhZsgNTvxSOygl4fkYDxQRsZs6kETD3h7ekvZFhFs4EWTUEXpbVQKIA2mVrVI7A81MX8BB4gURkNUQI72z7FILF7AG5MN7jfw%3D%3D" target='_blank'>View ERD</a></p>


## List API

1. Register Mahasiswa
- End Point : http://localhost:8000/api/register
- Raw Json :
{
    "nama" : "Yana",
    "nim" : "1234",
    "fakultas" : "TI",
    "jurusan" : "TI",
    "no_hp" : "080808",
    "no_wa" : "12312356",
    "email" : "yana@perpus.com",
    "password" : "yana12345"
}